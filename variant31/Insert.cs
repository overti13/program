﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace variant31
{
    public partial class Insert : Form
    {
        public Insert()
        {
            InitializeComponent();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Insert_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DB db = new DB();
                SqlCommand command = new SqlCommand("INSERT INTO [Users] ([Секция], [Фамилия], [Имя], [Отчество], [Дата], [Фамилия(родителя)], [Имя(родителя)], [Отчество(родителя)], [Номер], [Стоимость], [Задолженность])VALUES(@Section, @SurnameChild, @NameChild, @FatherNChild, @DateChild, @SurnameParent, @NameParent, @FatherNParent, @TelNumber, @Coast, @Debt)", db.GetConnection());
                
                command.Parameters.AddWithValue("Section", section.SelectedItem);
                command.Parameters.AddWithValue("SurnameChild", textBox1.Text);
                command.Parameters.AddWithValue("NameChild", textBox2.Text);
                command.Parameters.AddWithValue("FatherNChild", textBox3.Text);
                command.Parameters.AddWithValue("DateChild", dateBatheday.Value);
                command.Parameters.AddWithValue("SurnameParent", textBox6.Text);
                command.Parameters.AddWithValue("NameParent", textBox5.Text);
                command.Parameters.AddWithValue("FatherNParent", textBox4.Text);
                command.Parameters.AddWithValue("TelNumber", maskedPhoneNumber.Text);
                command.Parameters.AddWithValue("Coast", textBox9.Text);
                command.Parameters.AddWithValue("Debt", textBox10.Text);
                db.OpenDB();
                command.ExecuteNonQuery();
                db.CloseDB();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void назадToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void section_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dateBatheday_ValueChanged(object sender, EventArgs e)
        {

        }

        private void maskedPhoneNumber_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox6_Click(object sender, EventArgs e)
        {
            textBox6.Text = textBox1.Text;
        }
    }
}
