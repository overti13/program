﻿namespace variant31
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.учётныеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.maskedPhoneNumber = new System.Windows.Forms.MaskedTextBox();
            this.dateBatheday = new System.Windows.Forms.DateTimePicker();
            this.section = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.update1 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.databaseUsersDataSet = new variant31.DatabaseUsersDataSet();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter = new variant31.DatabaseUsersDataSetTableAdapters.UsersTableAdapter();
            this.databaseUsersDataSet1 = new variant31.DatabaseUsersDataSet();
            this.databaseUsersDataSet11 = new variant31.DatabaseUsersDataSet1();
            this.accountsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.accountsTableAdapter = new variant31.DatabaseUsersDataSet1TableAdapters.AccountsTableAdapter();
            this.databaseUsersDataSet2 = new variant31.DatabaseUsersDataSet2();
            this.usersBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter1 = new variant31.DatabaseUsersDataSet2TableAdapters.UsersTableAdapter();
            this.databaseUsersDataSet2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.databaseUsersDataSet11BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.databaseUsersDataSet2BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.databaseUsersDataSet00 = new variant31.DatabaseUsersDataSet00();
            this.usersBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter2 = new variant31.DatabaseUsersDataSet00TableAdapters.UsersTableAdapter();
            this.databaseUsersDataSet22 = new variant31.DatabaseUsersDataSet22();
            this.usersBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter3 = new variant31.DatabaseUsersDataSet22TableAdapters.UsersTableAdapter();
            this.databaseUsersDataSet22BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.databaseUsersDataSet22BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.databaseUsersDataSet3333 = new variant31.DatabaseUsersDataSet3333();
            this.accountsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.accountsTableAdapter1 = new variant31.DatabaseUsersDataSet3333TableAdapters.AccountsTableAdapter();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet11BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet2BindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet22BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet22BindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet3333)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountsBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem1,
            this.учётныеToolStripMenuItem,
            this.обновитьToolStripMenuItem,
            this.добавитьToolStripMenuItem,
            this.справкаToolStripMenuItem,
            this.выходToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1250, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem1
            // 
            this.файлToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сохранитьToolStripMenuItem});
            this.файлToolStripMenuItem1.Name = "файлToolStripMenuItem1";
            this.файлToolStripMenuItem1.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem1.Text = "Файл";
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // учётныеToolStripMenuItem
            // 
            this.учётныеToolStripMenuItem.Name = "учётныеToolStripMenuItem";
            this.учётныеToolStripMenuItem.Size = new System.Drawing.Size(107, 20);
            this.учётныеToolStripMenuItem.Text = "Учётные записи";
            this.учётныеToolStripMenuItem.Click += new System.EventHandler(this.файлToolStripMenuItem_Click);
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.обновитьToolStripMenuItem.Text = "Обновить";
            this.обновитьToolStripMenuItem.Click += new System.EventHandler(this.обновитьToolStripMenuItem_Click);
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.добавитьToolStripMenuItem.Text = "Добавить";
            this.добавитьToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem1
            // 
            this.выходToolStripMenuItem1.Name = "выходToolStripMenuItem1";
            this.выходToolStripMenuItem1.Size = new System.Drawing.Size(54, 20);
            this.выходToolStripMenuItem1.Text = "Выход";
            this.выходToolStripMenuItem1.Click += new System.EventHandler(this.выходToolStripMenuItem1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1250, 300);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1242, 274);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Пользователи";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1236, 268);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.maskedPhoneNumber);
            this.tabPage2.Controls.Add(this.dateBatheday);
            this.tabPage2.Controls.Add(this.section);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.update1);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.textBox10);
            this.tabPage2.Controls.Add(this.textBox9);
            this.tabPage2.Controls.Add(this.textBox4);
            this.tabPage2.Controls.Add(this.textBox5);
            this.tabPage2.Controls.Add(this.textBox6);
            this.tabPage2.Controls.Add(this.textBox3);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.textBox8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1242, 274);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Изменить";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // maskedPhoneNumber
            // 
            this.maskedPhoneNumber.Location = new System.Drawing.Point(164, 175);
            this.maskedPhoneNumber.Mask = "+375 (00) 000-0000";
            this.maskedPhoneNumber.Name = "maskedPhoneNumber";
            this.maskedPhoneNumber.Size = new System.Drawing.Size(123, 20);
            this.maskedPhoneNumber.TabIndex = 78;
            // 
            // dateBatheday
            // 
            this.dateBatheday.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateBatheday.Location = new System.Drawing.Point(8, 175);
            this.dateBatheday.MaxDate = new System.DateTime(2020, 6, 2, 0, 0, 0, 0);
            this.dateBatheday.Name = "dateBatheday";
            this.dateBatheday.Size = new System.Drawing.Size(99, 20);
            this.dateBatheday.TabIndex = 77;
            this.dateBatheday.Value = new System.DateTime(2020, 6, 2, 0, 0, 0, 0);
            // 
            // section
            // 
            this.section.FormattingEnabled = true;
            this.section.Items.AddRange(new object[] {
            "Футбод",
            "Гандбол",
            "Плавание",
            "Художественная гимнастика",
            "Бокс",
            "Лёгкая атлетика",
            "Тяжёлая атлетика"});
            this.section.Location = new System.Drawing.Point(8, 25);
            this.section.Name = "section";
            this.section.Size = new System.Drawing.Size(123, 21);
            this.section.TabIndex = 76;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 75;
            this.label12.Text = "Секция";
            // 
            // update1
            // 
            this.update1.Location = new System.Drawing.Point(8, 201);
            this.update1.Name = "update1";
            this.update1.Size = new System.Drawing.Size(83, 25);
            this.update1.TabIndex = 74;
            this.update1.Text = "Изменить";
            this.update1.UseVisualStyleBackColor = true;
            this.update1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(161, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 73;
            this.label11.Text = "Родитель";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(328, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 72;
            this.label10.Text = "Задолженность";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(328, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 71;
            this.label9.Text = "Стоимость";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(161, 159);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 70;
            this.label8.Text = "Номер тел.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(161, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 69;
            this.label7.Text = "Отчество";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(161, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 68;
            this.label6.Text = "Имя";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(161, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 67;
            this.label5.Text = "Фамилия";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 66;
            this.label4.Text = "Дата";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 65;
            this.label3.Text = "Отчество";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 64;
            this.label2.Text = "Имя";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "Фамилия";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(331, 101);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(84, 20);
            this.textBox10.TabIndex = 62;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(331, 64);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(84, 20);
            this.textBox9.TabIndex = 61;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(164, 139);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(123, 20);
            this.textBox4.TabIndex = 60;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(164, 101);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(123, 20);
            this.textBox5.TabIndex = 59;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(164, 64);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(123, 20);
            this.textBox6.TabIndex = 58;
            this.textBox6.Click += new System.EventHandler(this.textBox6_Click);
            this.textBox6.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(8, 139);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(123, 20);
            this.textBox3.TabIndex = 57;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(8, 101);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(123, 20);
            this.textBox2.TabIndex = 56;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(8, 64);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(123, 20);
            this.textBox8.TabIndex = 55;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1242, 274);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Должники";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(1242, 274);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // databaseUsersDataSet
            // 
            this.databaseUsersDataSet.DataSetName = "DatabaseUsersDataSet";
            this.databaseUsersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataMember = "Users";
            this.usersBindingSource.DataSource = this.databaseUsersDataSet;
            // 
            // usersTableAdapter
            // 
            this.usersTableAdapter.ClearBeforeFill = true;
            // 
            // databaseUsersDataSet1
            // 
            this.databaseUsersDataSet1.DataSetName = "DatabaseUsersDataSet";
            this.databaseUsersDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // databaseUsersDataSet11
            // 
            this.databaseUsersDataSet11.DataSetName = "DatabaseUsersDataSet1";
            this.databaseUsersDataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // accountsBindingSource
            // 
            this.accountsBindingSource.DataMember = "Accounts";
            this.accountsBindingSource.DataSource = this.databaseUsersDataSet11;
            // 
            // accountsTableAdapter
            // 
            this.accountsTableAdapter.ClearBeforeFill = true;
            // 
            // databaseUsersDataSet2
            // 
            this.databaseUsersDataSet2.DataSetName = "DatabaseUsersDataSet2";
            this.databaseUsersDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usersBindingSource1
            // 
            this.usersBindingSource1.DataMember = "Users";
            this.usersBindingSource1.DataSource = this.databaseUsersDataSet2;
            // 
            // usersTableAdapter1
            // 
            this.usersTableAdapter1.ClearBeforeFill = true;
            // 
            // databaseUsersDataSet2BindingSource
            // 
            this.databaseUsersDataSet2BindingSource.DataSource = this.databaseUsersDataSet2;
            this.databaseUsersDataSet2BindingSource.Position = 0;
            // 
            // databaseUsersDataSet11BindingSource
            // 
            this.databaseUsersDataSet11BindingSource.DataSource = this.databaseUsersDataSet11;
            this.databaseUsersDataSet11BindingSource.Position = 0;
            // 
            // databaseUsersDataSet2BindingSource1
            // 
            this.databaseUsersDataSet2BindingSource1.DataSource = this.databaseUsersDataSet2;
            this.databaseUsersDataSet2BindingSource1.Position = 0;
            // 
            // databaseUsersDataSet00
            // 
            this.databaseUsersDataSet00.DataSetName = "DatabaseUsersDataSet00";
            this.databaseUsersDataSet00.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usersBindingSource2
            // 
            this.usersBindingSource2.DataMember = "Users";
            this.usersBindingSource2.DataSource = this.databaseUsersDataSet00;
            // 
            // usersTableAdapter2
            // 
            this.usersTableAdapter2.ClearBeforeFill = true;
            // 
            // databaseUsersDataSet22
            // 
            this.databaseUsersDataSet22.DataSetName = "DatabaseUsersDataSet22";
            this.databaseUsersDataSet22.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usersBindingSource3
            // 
            this.usersBindingSource3.DataMember = "Users";
            this.usersBindingSource3.DataSource = this.databaseUsersDataSet22;
            // 
            // usersTableAdapter3
            // 
            this.usersTableAdapter3.ClearBeforeFill = true;
            // 
            // databaseUsersDataSet22BindingSource
            // 
            this.databaseUsersDataSet22BindingSource.DataSource = this.databaseUsersDataSet22;
            this.databaseUsersDataSet22BindingSource.Position = 0;
            // 
            // databaseUsersDataSet22BindingSource1
            // 
            this.databaseUsersDataSet22BindingSource1.DataSource = this.databaseUsersDataSet22;
            this.databaseUsersDataSet22BindingSource1.Position = 0;
            // 
            // databaseUsersDataSet3333
            // 
            this.databaseUsersDataSet3333.DataSetName = "DatabaseUsersDataSet3333";
            this.databaseUsersDataSet3333.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // accountsBindingSource1
            // 
            this.accountsBindingSource1.DataMember = "Accounts";
            this.accountsBindingSource1.DataSource = this.databaseUsersDataSet3333;
            // 
            // accountsTableAdapter1
            // 
            this.accountsTableAdapter1.ClearBeforeFill = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1027, 0);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(116, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Секция",
            "Фамилия",
            "Имя"});
            this.comboBox1.Location = new System.Drawing.Point(944, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(77, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.Text = "Поиск";
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            this.справкаToolStripMenuItem.Click += new System.EventHandler(this.справкаToolStripMenuItem_Click);
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1250, 324);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Admin";
            this.Text = "Admin";
            this.Activated += new System.EventHandler(this.Admin_Load);
            this.Load += new System.EventHandler(this.Admin_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet11BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet2BindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet22BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet22BindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseUsersDataSet3333)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountsBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem учётныеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DatabaseUsersDataSet databaseUsersDataSet;
        private System.Windows.Forms.BindingSource usersBindingSource;
        private DatabaseUsersDataSetTableAdapters.UsersTableAdapter usersTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private DatabaseUsersDataSet databaseUsersDataSet1;
        private DatabaseUsersDataSet1 databaseUsersDataSet11;
        private System.Windows.Forms.BindingSource accountsBindingSource;
        private DatabaseUsersDataSet1TableAdapters.AccountsTableAdapter accountsTableAdapter;
        private DatabaseUsersDataSet2 databaseUsersDataSet2;
        private System.Windows.Forms.BindingSource usersBindingSource1;
        private DatabaseUsersDataSet2TableAdapters.UsersTableAdapter usersTableAdapter1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource databaseUsersDataSet2BindingSource;
        private System.Windows.Forms.BindingSource databaseUsersDataSet11BindingSource;
        private System.Windows.Forms.BindingSource databaseUsersDataSet2BindingSource1;
        private DatabaseUsersDataSet00 databaseUsersDataSet00;
        private System.Windows.Forms.BindingSource usersBindingSource2;
        private DatabaseUsersDataSet00TableAdapters.UsersTableAdapter usersTableAdapter2;
        private DatabaseUsersDataSet22 databaseUsersDataSet22;
        private System.Windows.Forms.BindingSource usersBindingSource3;
        private DatabaseUsersDataSet22TableAdapters.UsersTableAdapter usersTableAdapter3;
        private System.Windows.Forms.BindingSource databaseUsersDataSet22BindingSource;
        private System.Windows.Forms.BindingSource databaseUsersDataSet22BindingSource1;
        private DatabaseUsersDataSet3333 databaseUsersDataSet3333;
        private System.Windows.Forms.BindingSource accountsBindingSource1;
        private DatabaseUsersDataSet3333TableAdapters.AccountsTableAdapter accountsTableAdapter1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MaskedTextBox maskedPhoneNumber;
        private System.Windows.Forms.DateTimePicker dateBatheday;
        private System.Windows.Forms.ComboBox section;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button update1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
    }
}