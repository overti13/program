﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Linq.Expressions;

namespace variant31
{
    public partial class Admin : Form
    {
        int k = 0;

        private SqlCommandBuilder sqlBuilder = null;

        private SqlCommandBuilder sqlBuilder2 = null;

        private SqlDataAdapter sqlDataAdapter = null;

        private SqlDataAdapter sqlDataAdapter2 = null;

        private DataSet dataSet = null;

        private DataTable tbl;

        public Admin()
        {
            InitializeComponent();
        }

        public Admin(Start k)
        {
            InitializeComponent();
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            DB db = new DB();
            db.OpenDB();
            LoadData("Users");
            LoadData2();
            db.CloseDB();
        }

        private void LoadData(string table)
        {
            DB db = new DB();
            try
            {
                
                sqlDataAdapter = new SqlDataAdapter("SELECT *, 'Delete' AS [Command] FROM Users", db.GetConnection());
                
                sqlBuilder = new SqlCommandBuilder(sqlDataAdapter);
                
                sqlBuilder.GetDeleteCommand();

                dataSet = new DataSet();

                sqlDataAdapter.Fill(dataSet, table);

                dataGridView1.DataSource = dataSet.Tables[table];

                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();

                    dataGridView1[12, i] = linkCell;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadData2()
        {
            DB db = new DB();
            try
            {
                sqlDataAdapter2 = new SqlDataAdapter("SELECT * FROM[Users] WHERE[Задолженность] > 0", db.GetConnection());
        
                sqlBuilder2 = new SqlCommandBuilder(sqlDataAdapter2);

                tbl = new DataTable();
        
                sqlDataAdapter2.Fill(tbl);
        
                dataGridView2.DataSource = tbl;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ReloadData(string table)
        {
            DB db = new DB();
            try
            {
                dataSet.Tables[table].Clear();

                sqlDataAdapter.Fill(dataSet, table);

                dataGridView1.DataSource = dataSet.Tables[table];

                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();

                    dataGridView1[12, i] = linkCell;
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ReloadData2(string table)
        {
            DB db = new DB();
            try
            {
                tbl.Clear();

                sqlDataAdapter2.Fill(tbl);


                dataGridView2.DataSource = tbl;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void обновитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DB db = new DB();
            db.OpenDB();
            ReloadData("Users");
            ReloadData2("Users");
            db.CloseDB();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            k = e.RowIndex;
            DB db = new DB();
            try
            {
                if (e.ColumnIndex == 12)
                {
                    string task = dataGridView1.Rows[e.RowIndex].Cells[12].Value.ToString();
                    switch (task)
                    {
                        case "Delete":
                            Delete(e);
                            break;
                    }
                    db.OpenDB();
                    ReloadData("Users");
                    db.CloseDB();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column_KeyPress);

            if (dataGridView1.CurrentCell.ColumnIndex == 9 || dataGridView1.CurrentCell.ColumnIndex == 5 || dataGridView1.CurrentCell.ColumnIndex == 10 || dataGridView1.CurrentCell.ColumnIndex == 11)
            {
                TextBox textBox = e.Control as TextBox;

                if (textBox != null)
                {
                    textBox.KeyPress += new KeyPressEventHandler(Column_KeyPress);
                }
            }
        }

        private void Column_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Insert ins = new Insert();
            ins.ShowDialog();
        }

        private void Delete(DataGridViewCellEventArgs e)
        {
            if (MessageBox.Show("Удалить эту строку?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int rowIndex = e.RowIndex;

                dataGridView1.Rows.RemoveAt(rowIndex);

                dataSet.Tables["Users"].Rows[rowIndex].Delete();

                sqlDataAdapter.Update(dataSet, "Users");
            }
        }

        private void файлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Accounts acc = new Accounts();
            acc.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DataView DV = new DataView(dataSet.Tables["Users"]);
            if (comboBox1.SelectedIndex == 0)
                DV.RowFilter = String.Format("Секция LIKE '%{0}%'", textBox1.Text);
            else if (comboBox1.SelectedIndex == 1)
                DV.RowFilter = String.Format("Фамилия LIKE '%{0}%'", textBox1.Text);
            else if (comboBox1.SelectedIndex == 2)
                DV.RowFilter = String.Format("Имя LIKE '%{0}%'", textBox1.Text);
            dataGridView1.DataSource = DV;
        }

        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DB db = new DB();
            if (MessageBox.Show("Выйти из программы?", "Выход", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                db.CloseDB();
                this.Hide();
                Authentification auth = new Authentification();
                auth.ShowDialog();
                this.Close();
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sv = new SaveFileDialog();
            sv.Filter = "Text File|*.txt";
            sv.FileName = "Данные";
            sv.Title = "Сохранить данные";
            if (sv.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = sv.FileName;
                TextWriter tw = new StreamWriter(File.Create(path));
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dataGridView1.Columns.Count - 1; j++)
                    {
                        tw.Write($"{dataGridView1.Rows[i].Cells[j].Value}  ");
                    }
                    tw.WriteLine();
                }
                tw.Dispose();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                k = Math.Abs(e.RowIndex);
                if (e.RowIndex < 0)
                {

                }
                section.SelectedItem = dataGridView1.Rows[k].Cells["Секция"].Value;
                textBox8.Text = (string)dataGridView1.Rows[k].Cells["Фамилия"].Value;
                textBox2.Text = (string)dataGridView1.Rows[k].Cells["Имя"].Value;
                textBox3.Text = (string)dataGridView1.Rows[k].Cells["Отчество"].Value;
                dateBatheday.Value = (DateTime)dataGridView1.Rows[k].Cells["Дата"].Value;
                textBox6.Text = (string)dataGridView1.Rows[k].Cells["Фамилия(родителя)"].Value;
                textBox5.Text = (string)dataGridView1.Rows[k].Cells["Имя(родителя)"].Value;
                textBox4.Text = (string)dataGridView1.Rows[k].Cells["Отчество(родителя)"].Value;
                maskedPhoneNumber.Text = (string)dataGridView1.Rows[k].Cells["Номер"].Value;
                textBox9.Text = dataGridView1.Rows[k].Cells["Стоимость"].Value.ToString();
                textBox10.Text = dataGridView1.Rows[k].Cells["Задолженность"].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                dataSet.Tables["Users"].Rows[k]["Секция"] = section.SelectedItem;
                dataSet.Tables["Users"].Rows[k]["Фамилия"] = textBox8.Text;
                dataSet.Tables["Users"].Rows[k]["Имя"] = textBox2.Text;
                dataSet.Tables["Users"].Rows[k]["Отчество"] = textBox3.Text;
                dataSet.Tables["Users"].Rows[k]["Дата"] = dateBatheday.Text;
                dataSet.Tables["Users"].Rows[k]["Фамилия(родителя)"] = textBox6.Text;
                dataSet.Tables["Users"].Rows[k]["Имя(родителя)"] = textBox5.Text;
                dataSet.Tables["Users"].Rows[k]["Отчество(родителя)"] = textBox4.Text;
                dataSet.Tables["Users"].Rows[k]["Номер"] = maskedPhoneNumber.Text;
                dataSet.Tables["Users"].Rows[k]["Стоимость"] = textBox9.Text;
                dataSet.Tables["Users"].Rows[k]["Задолженность"] = textBox10.Text;

                sqlDataAdapter.Update(dataSet, "Users");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //k = e.RowIndex;
            DB db = new DB();
            try
            {
                if (e.ColumnIndex == 12)
                {
                    string task = dataGridView1.Rows[e.RowIndex].Cells[12].Value.ToString();
                    switch (task)
                    {
                        case "Delete":
                            Delete(e);
                            break;
                    }
                    db.OpenDB();
                    ReloadData("Users");
                    ReloadData2("Users");
                    db.CloseDB();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            k = Math.Abs(e.RowIndex);
            section.SelectedItem = dataGridView1.Rows[k].Cells["Секция"].Value;
            textBox8.Text = (string)dataGridView1.Rows[k].Cells["Фамилия"].Value;
            textBox2.Text = (string)dataGridView1.Rows[k].Cells["Имя"].Value;
            textBox3.Text = (string)dataGridView1.Rows[k].Cells["Отчество"].Value;
            dateBatheday.Value = (DateTime)dataGridView1.Rows[k].Cells["Дата"].Value;
            textBox6.Text = (string)dataGridView1.Rows[k].Cells["Фамилия(родителя)"].Value;
            textBox5.Text = (string)dataGridView1.Rows[k].Cells["Имя(родителя)"].Value;
            textBox4.Text = (string)dataGridView1.Rows[k].Cells["Отчество(родителя)"].Value;
            maskedPhoneNumber.Text = (string)dataGridView1.Rows[k].Cells["Номер"].Value;
            textBox9.Text = dataGridView1.Rows[k].Cells["Стоимость"].Value.ToString();
            textBox10.Text = dataGridView1.Rows[k].Cells["Задолженность"].Value.ToString();
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_Click(object sender, EventArgs e)
        {
            textBox6.Text = textBox8.Text;
        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Info inf = new Info();
            inf.ShowDialog();
        }
    }
}
