﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace variant31
{
    public partial class User : Form
    {
        private SqlDataAdapter sqlDataAdapter = null;

        private SqlCommandBuilder sqlBuilder2 = null;

        private SqlCommandBuilder sqlBuilder = null;

        private SqlDataAdapter sqlDataAdapter2 = null;

        private DataSet dataSet = null;

        private DataTable tbl;

        public User()
        {
            InitializeComponent();
        }

        public User(Start f)
        {
            InitializeComponent();
        }

        private void User_Load(object sender, EventArgs e)
        {
            DB db = new DB();
            db.OpenDB();
            LoadData("Users");
            LoadData2();
            db.CloseDB();
        }

        private void LoadData(string table)
        {
            DB db = new DB();
            try
            {

                sqlDataAdapter = new SqlDataAdapter("SELECT * FROM [Users]", db.GetConnection());

                sqlBuilder = new SqlCommandBuilder(sqlDataAdapter);

                sqlBuilder.GetInsertCommand();
                sqlBuilder.GetUpdateCommand();
                sqlBuilder.GetDeleteCommand();

                dataSet = new DataSet();

                sqlDataAdapter.Fill(dataSet, table);

                dataGridView1.DataSource = dataSet.Tables[table];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadData2()
        {
            DB db = new DB();
            try
            {
                sqlDataAdapter2 = new SqlDataAdapter("SELECT * FROM[Users] WHERE[Задолженность] > 0", db.GetConnection());

                sqlBuilder2 = new SqlCommandBuilder(sqlDataAdapter2);

                tbl = new DataTable();

                sqlDataAdapter2.Fill(tbl);

                dataGridView2.DataSource = tbl;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ReloadData(string table)
        {
            DB db = new DB();
            try
            {
                dataSet.Tables[table].Clear();

                sqlDataAdapter.Fill(dataSet, table);

                dataGridView1.DataSource = dataSet.Tables[table];

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ReloadData2(string table)
        {
            DB db = new DB();
            try
            {
                tbl.Clear();

                sqlDataAdapter2.Fill(tbl);


                dataGridView2.DataSource = tbl;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void обновитьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            DB db = new DB();
            db.OpenDB();
            ReloadData("Users");
            db.CloseDB();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DB db = new DB();
            if (MessageBox.Show("Выйти из программы?", "Выход", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                db.CloseDB();
                this.Hide();
                Authentification auth = new Authentification();
                auth.ShowDialog();
                this.Close();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DataView DV = new DataView(dataSet.Tables["Users"]);
            if (comboBox1.SelectedIndex == 0)
                DV.RowFilter = String.Format("Секция LIKE '%{0}%'", textBox1.Text);
            else if (comboBox1.SelectedIndex == 1)
                DV.RowFilter = String.Format("Фамилия LIKE '%{0}%'", textBox1.Text);
            else if (comboBox1.SelectedIndex == 2)
                DV.RowFilter = String.Format("Имя LIKE '%{0}%'", textBox1.Text);
            dataGridView1.DataSource = DV;
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sv = new SaveFileDialog();
            sv.Filter = "Text File|*.txt";
            sv.FileName = "Данные";
            sv.Title = "Сохранить данные";
            if (sv.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = sv.FileName;
                TextWriter tw = new StreamWriter(File.Create(path));
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dataGridView1.Columns.Count - 1; j++)
                    {
                        tw.Write($"{dataGridView1.Rows[i].Cells[j].Value}  ");
                        Console.WriteLine(dataGridView1.Rows[i].Cells[j].Value);
                    }
                    tw.WriteLine();
                }
                tw.Dispose();
            }
        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Info inf = new Info();
            inf.ShowDialog();
        }
    }
}
