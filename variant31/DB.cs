﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace variant31
{
    class DB
    {
        SqlConnection sqlConnection = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=D:\Programs\New\variant31\variant31\DatabaseUsers.mdf;Integrated Security = True; Connect Timeout = 30");
        public void OpenDB() 
        {
            if (sqlConnection.State == System.Data.ConnectionState.Closed)
                sqlConnection.Open();
        }

        public void CloseDB()
        {
            if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed)
                sqlConnection.Close();
        }

        public SqlConnection GetConnection()
        {
            
            return sqlConnection;
        }
    }
}
