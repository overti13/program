﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace variant31
{
    public partial class Authentification : Form
    {

        public Authentification()
        {
            InitializeComponent();
        }

        public Authentification(Start f)
        {
            InitializeComponent();
        }

        private void Authentification_Load(object sender, EventArgs e)
        {
            
        }

        private void Enter_Click(object sender, EventArgs e)
        {
            String loginUser = loginField.Text;
            String passUser = passField.Text;
            
            DB db = new DB();
            
            DataTable table = new DataTable();
            
            SqlDataAdapter adapter = new SqlDataAdapter();
            
            string sql = "SELECT * FROM [Accounts] WHERE [Login] = @lU AND [Password] = @pU";
            
            SqlCommand commandUser = new SqlCommand(sql, db.GetConnection());
            commandUser.Parameters.AddWithValue("@lU", loginUser);
            commandUser.Parameters.AddWithValue("@pU", passUser);
            
            adapter.SelectCommand = commandUser;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                UserRole();
            }
            else
            {
                passField.Text = "";
                MessageBox.Show("Не правильно введён пароль или логин!");
            }
        }

        public void UserRole()
        {
            int role = 1;
            DB db = new DB();
            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = "SELECT * FROM [Accounts] WHERE [Login] = @lU AND [Role] = @ad";
            SqlCommand commandUser = new SqlCommand(sql, db.GetConnection());
            commandUser.Parameters.AddWithValue("@ad", role);
            commandUser.Parameters.AddWithValue("@lU", loginField.Text);
            adapter.SelectCommand = commandUser;
            adapter.Fill(table);
            if (table.Rows.Count > 0)
            {
                this.Hide();
                Admin admin = new Admin();
                admin.ShowDialog();
            }else
            {
                this.Hide();
                User user = new User();
                user.ShowDialog();
            }
        }

        private void loginField_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
