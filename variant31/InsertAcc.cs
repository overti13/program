﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace variant31
{
    public partial class InsertAcc : Form
    {
        public InsertAcc()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DB db = new DB();
                SqlCommand command = new SqlCommand("INSERT INTO [Accounts] (Login, Password, Role)VALUES(@Login, @Password, @Role)", db.GetConnection());

                DataTable table = new DataTable();

                SqlDataAdapter adapter = new SqlDataAdapter();

                string sql = "SELECT * FROM [Accounts] WHERE [Login] = @lU";

                SqlCommand commandUser = new SqlCommand(sql, db.GetConnection());
                commandUser.Parameters.AddWithValue("@lU", textBox11.Text);
                

                adapter.SelectCommand = commandUser;
                adapter.Fill(table);

                if (table.Rows.Count > 0)
                {
                    MessageBox.Show("Такой логин уже существует!");
                }
                else
                {
                    command.Parameters.AddWithValue("Login", textBox11.Text);
                    command.Parameters.AddWithValue("Password", textBox1.Text);
                }

                if (radioButton1.Checked == true && radioButton2.Checked == true)
                    command.Parameters.AddWithValue("Role", 1);
                else if ((radioButton2.Checked == true && radioButton2.Checked == true))
                    command.Parameters.AddWithValue("Role", 0);
                else
                    MessageBox.Show("Выбирите роль нового пользователя");
                db.OpenDB();
                command.ExecuteNonQuery();
                db.CloseDB();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void InsertAcc_Load(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void назадToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
