﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace variant31
{
    public partial class Accounts : Form
    {
        private SqlCommandBuilder sqlBuilder = null;

        private SqlDataAdapter sqlDataAdapter = null;

        private DataSet dataSet = null;

        private bool newRowAdding = false;

        public Accounts()
        {
            InitializeComponent();
        }

        public Accounts(Start k)
        {
            InitializeComponent();
        }

        private void Accounts_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "databaseUsersDataSet0000000000.Accounts". При необходимости она может быть перемещена или удалена.
            this.accountsTableAdapter.Fill(this.databaseUsersDataSet0000000000.Accounts);
            DB db = new DB();
            db.OpenDB();
            LoadData2();
            db.CloseDB();
        }

        private void LoadData2()
        {
            DB db = new DB();

            try
            {
                sqlDataAdapter = new SqlDataAdapter("SELECT *, 'Delete' AS [Command] FROM Accounts", db.GetConnection());

                sqlBuilder = new SqlCommandBuilder(sqlDataAdapter);

                sqlBuilder.GetInsertCommand();
                sqlBuilder.GetUpdateCommand();
                sqlBuilder.GetDeleteCommand();

                dataSet = new DataSet();

                sqlDataAdapter.Fill(dataSet, "Accounts");

                dataGridView2.DataSource = dataSet.Tables["Accounts"];

                for (int i = 0; i < dataGridView2.Rows.Count; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();

                    dataGridView2[4, i] = linkCell;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ReloadData1()
        {
            DB db = new DB();
            try
            {
                dataSet.Tables["Accounts"].Clear();

                sqlDataAdapter.Fill(dataSet, "Accounts");

                dataGridView2.DataSource = dataSet.Tables["Accounts"];

                for (int i = 0; i < dataGridView2.Rows.Count; i++)
                {
                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();

                    dataGridView2[4, i] = linkCell;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DB db = new DB();
            try
            {
                if (e.ColumnIndex == 4)
                {
                    string task = dataGridView2.Rows[e.RowIndex].Cells[4].Value.ToString();
                    switch (task)
                    {
                        case "Delete":
                            Delete2(e);
                            break;
                        case "Update":
                            Update2(e);
                            break;
                    }
                    db.OpenDB();
                    ReloadData1();
                    db.CloseDB();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Column_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Delete2(DataGridViewCellEventArgs e)
        {
            if (MessageBox.Show("Удалить эту строку?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int rowIndex = e.RowIndex;

                dataGridView2.Rows.RemoveAt(rowIndex);

                dataSet.Tables["Accounts"].Rows[rowIndex].Delete();

                sqlDataAdapter.Update(dataSet, "Accounts");
            }
        }

        private void Update2(DataGridViewCellEventArgs e)
        {
            int r = e.RowIndex;

            dataSet.Tables["Accounts"].Rows[r]["Login"] = dataGridView2.Rows[r].Cells["Login"].Value;
            dataSet.Tables["Accounts"].Rows[r]["Password"] = dataGridView2.Rows[r].Cells["Password"].Value;
            dataSet.Tables["Accounts"].Rows[r]["Role"] = dataGridView2.Rows[r].Cells["Role"].Value;

            sqlDataAdapter.Update(dataSet, "Accounts");

            dataGridView2.Rows[e.RowIndex].Cells[4].Value = "Update";
        }

        private void обновитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DB db = new DB();
            db.OpenDB();
            ReloadData1();
            db.CloseDB();
        }

        private void dataGridView2_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (newRowAdding == false)
                {
                    int rowIndex = dataGridView2.SelectedCells[0].RowIndex;

                    DataGridViewRow editingRow = dataGridView2.Rows[rowIndex];

                    DataGridViewLinkCell linkCell = new DataGridViewLinkCell();

                    dataGridView2[4, rowIndex] = linkCell;

                    editingRow.Cells["Command"].Value = "Update";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView2_EditingControlShowing_1(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column_KeyPress);

            if (dataGridView2.CurrentCell.ColumnIndex == 3)
            {
                TextBox textBox = e.Control as TextBox;

                if (textBox != null)
                {
                    textBox.KeyPress += new KeyPressEventHandler(Column_KeyPress);
                }
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InsertAcc inc = new InsertAcc();
            inc.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DataView DV = new DataView(dataSet.Tables["Accounts"]);
            if (comboBox1.SelectedIndex == 0)
                DV.RowFilter = String.Format("Login LIKE '%{0}%'", textBox1.Text);
            dataGridView2.DataSource = DV;
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DB db = new DB();
            if (MessageBox.Show("Выйти из программы?", "Выход", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                db.CloseDB();
                this.Close();
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sv = new SaveFileDialog();
            sv.Filter = "Text File|*.txt";
            sv.FileName = "Данные";
            sv.Title = "Сохранить данные";
            if (sv.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = sv.FileName;
                TextWriter tw = new StreamWriter(File.Create(path));
                for (int i = 0; i < dataGridView2.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dataGridView2.Columns.Count - 1; j++)
                    {
                        tw.Write($"{dataGridView2.Rows[i].Cells[j].Value}  ");
                        Console.WriteLine(dataGridView2.Rows[i].Cells[j].Value);
                    }
                    tw.WriteLine();
                }
                tw.Dispose();
            }
                
        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Info inf = new Info();
            inf.ShowDialog();
        }
    }
}
